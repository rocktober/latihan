<div>
    <form wire:submit.prevent="update">
        <input type="hidden" wire:model="contactId">
        <div class="form-group">
            <div class="row">
                <div class="col-6">
                    <input wire:model="name" type="text" name="name"
                        class="form-control @error('name') is-invalid @enderror" placeholder="e.g: Name">
                    @error('name')
                        <span class="invalid-feedback">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="col-6">
                    <input wire:model="phone" type="text" name="phone"
                        class="form-control @error('phone') is-invalid @enderror" placeholder="e.g: Phone">
                    @error('phone')
                        <span class="invalid-feedback">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>
        </div>
        <button type="submit" class="btn btn-sm btn-primary mt-2">Submit</button>
    </form>
</div>
