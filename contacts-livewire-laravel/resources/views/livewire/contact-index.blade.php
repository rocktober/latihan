<div>

    @if (session()->has('message'))
        <div class="alert alert-success text-center">
            {{ session('message') }}
        </div>
    @endif

    @if ($statusUpdate)
        @livewire('contact-update')
    @else
        @livewire('contact-create')
    @endif

    <hr>

    <div class="row">
        <div class="col">
            <select wire:model="paginate" class="form-select form-select-sm w-auto">
                <option value="5">5</option>
                <option value="10">10</option>
                <option value="15">15</option>
            </select>
        </div>
        <div class="col">
            <input wire:model="search" type="text" class="form-control form-control-sm" placeholder="Search">
        </div>
    </div>

    <hr>

    <table class="table table-hover">
        <thead class="table-dark">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Name</th>
                <th scope="col">Phone</th>
                <th scope="col"></th>
            </tr>
        </thead>
        <tbody>
            @foreach ($contacts as $index => $contact)
                <tr>
                    <th scope="row">{{ $index + $contacts->firstItem() }}</th>
                    <td>{{ $contact->name }}</td>
                    <td>{{ $contact->phone }}</td>
                    <td>
                        <button wire:click="getContact({{ $contact->id }})"
                            class="btn btn-sm btn-warning text-white">Edit</button>
                        <button wire:click="destroy({{ $contact->id }})"
                            class="btn btn-sm btn-danger text-white">Delete</button>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <div class="align-center d-flex justify-content-center">
        {{ $contacts->links() }}
    </div>
</div>
