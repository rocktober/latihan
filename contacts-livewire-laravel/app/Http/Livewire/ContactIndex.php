<?php

namespace App\Http\Livewire;

use App\Models\Contact;
use Livewire\Component;
use Livewire\WithPagination;

class ContactIndex extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';

    public $statusUpdate = false;
    public $paginate = 5;
    public $search;

    // protected $queryString = ['search'];

    protected $listeners = [
        'contactStored' => 'createContact',
        'contactUpdate' => 'updateContact'
    ];

    public function render()
    {
        return view('livewire.contact-index', [
            'contacts' => $this->search === null ?
                Contact::latest()->paginate($this->paginate) :
                Contact::latest()->where('name', 'like', '%' . $this->search . '%')->paginate($this->paginate)
        ]);
    }

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function getContact($id)
    {
        $this->statusUpdate = true;
        $contact = Contact::find($id);
        $this->emit('getContact', $contact);
    }

    public function destroy($id)
    {
        if ($id) {
            $data = Contact::find($id);
            $data->delete();
            session()->flash('message', 'Delete contact success!');
        }
    }

    public function createContact($contact)
    {
        // dd($contact);
        session()->flash('message', 'Create contact ' . $contact['name'] . ' success!');
    }

    public function updateContact($contact)
    {
        // dd($contact);
        session()->flash('message', 'Update contact ' . $contact['name'] . ' success!');
    }
}
