<div class="container-fluid">
    <div class="row">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header bg-dark text-white">
                    MapBox
                </div>
                <div class="card-body">
                    <div wire:ignore id='map' style='width: 100%; height: 70vh;'></div>

                    <div id="menu">
                        <input id="satellite-streets-v12" type="radio" name="rtoggle" value="satellite">
                        <!-- See a list of Mapbox-hosted public styles at -->
                        <!-- https://docs.mapbox.com/api/maps/styles/#mapbox-styles -->
                        <label for="satellite-streets-v12">satellite streets</label>
                        <input id="light-v11" type="radio" name="rtoggle" value="light">
                        <label for="light-v11">light</label>
                        <input id="dark-v11" type="radio" name="rtoggle" value="dark">
                        <label for="dark-v11">dark</label>
                        <input id="streets-v12" type="radio" name="rtoggle" value="streets">
                        <label for="streets-v12">streets</label>
                        <input id="outdoors-v12" type="radio" name="rtoggle" value="outdoors" checked="checked">
                        <label for="outdoors-v12">outdoors</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-header bg-dark text-white">
                    Form
                </div>
                <div class="card-body">
                    <form
                        @if ($isEdit) wire:submit.prevent="updateLocation"
                        @else
                            wire:submit.prevent="saveLocation" @endif>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Longtitude</label>
                                    <input wire:model="long" type="text" class="form-control"
                                        {{ $isEdit ? 'disabled' : null }}>
                                    @error('long')
                                        <small class="text-danger">{{ $message }}</small>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Lattitude</label>
                                    <input wire:model="lat" type="text" class="form-control"
                                        {{ $isEdit ? 'disabled' : null }}>
                                    @error('lat')
                                        <small class="text-danger">{{ $message }}</small>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="form-group mt-2">
                            <label>Title</label>
                            <input wire:model="title" type="text" class="form-control">
                            @error('title')
                                <small class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                        <div class="form-group mt-2">
                            <label>Description</label>
                            <textarea wire:model="description" class="form-control"></textarea>
                            @error('description')
                                <small class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                        <div class="form-group mt-2">
                            <label>Image</label>
                            <input wire:model="image" type="file" class="form-control">
                            @error('image')
                                <small class="text-danger">{{ $message }}</small>
                            @enderror
                            @if ($image)
                                <div class="mt-4">
                                    <img src="{{ $image->temporaryUrl() }}" class="img-fluid">
                                </div>
                            @endif
                            @if ($imageUrl && !$image)
                                <div class="mt-4">
                                    <img src="{{ asset('/storage/image/' . $imageUrl) }}" class="img-fluid">
                                </div>
                            @endif
                        </div>
                        <div class="form-group mt-4">
                            <button type="submit" class="btn btn-dark text-white btn-block"
                                style="width: 100%">{{ $isEdit ? 'Update Location' : 'Save Location' }}</button>
                            @if ($isEdit)
                                <button wire:click="deleteLocation" type="button"
                                    class="btn btn-danger text-white btn-block mt-2" style="width: 100%">Delete
                                    Location</button>
                            @endif
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@push('scripts')
    <script>
        document.addEventListener('livewire:load', () => {
            const defaultLocation = [110.39375746348406, -7.752939016083545]

            mapboxgl.accessToken = "{{ env('MAPBOX_KEY') }}";
            var map = new mapboxgl.Map({
                container: 'map',
                center: defaultLocation,
                zoom: 12,
                style: 'mapbox://styles/mapbox/streets-v12',
            });

            const loadLocations = (geoJson) => {
                geoJson.features.forEach((location) => {
                    const {
                        geometry,
                        properties
                    } = location

                    const {
                        iconSize,
                        locationId,
                        title,
                        image,
                        description
                    } = properties

                    let markerElement = document.createElement('div')
                    markerElement.className = 'marker' + locationId
                    markerElement.id = locationId
                    markerElement.style.backgroundImage = 'url(https://i.stack.imgur.com/A7tKh.png)'
                    markerElement.style.backgroundSize = 'cover'
                    markerElement.style.width = '50px'
                    markerElement.style.height = '50px'

                    const imageStorage = '{{ asset('/storage/image') }}' + '/' + image

                    const content = `
                    <div style="overflow-y, auto;max-height:400px,width:100%">
                        <table class="table table-sm mt-2">
                            <tbody>
                                <tr>
                                    <td>Title</td>
                                    <td>${title}</td>
                                </tr>
                                <tr>
                                    <td>Picture</td>
                                    <td><img src="${imageStorage}" loading="lazy" class="img-fluid" width="150px"></td>
                                </tr>
                                <tr>
                                    <td>Description</td>
                                    <td>${description}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    `

                    markerElement.addEventListener('click', (e) => {
                        const locationId = e.target.id
                        @this.findLocationById(locationId)
                    })

                    const popUp = new mapboxgl.Popup({
                        offset: 25
                    }).setHTML(content).setMaxWidth("400px")

                    new mapboxgl.Marker(markerElement)
                        .setLngLat(geometry.coordinates)
                        .setPopup(popUp)
                        .addTo(map)
                });
            }

            loadLocations({!! $geoJson !!})

            window.addEventListener('locationAdded', (e) => {
                loadLocations(JSON.parse(e.detail))
            })

            window.addEventListener('updateLocation', (e) => {
                loadLocations(JSON.parse(e.detail))
                $('.mapboxgl-popup').remove()
            })

            window.addEventListener('deleteLocation', (e) => {
                $('.marker' + e.detail).remove()
                $('.mapboxgl-popup').remove()
            })

            map.addControl(new mapboxgl.NavigationControl());

            const layerList = document.getElementById('menu');
            const inputs = layerList.getElementsByTagName('input');

            for (const input of inputs) {
                input.onclick = (layer) => {
                    const layerId = layer.target.id;
                    map.setStyle('mapbox://styles/mapbox/' + layerId);
                };
            }

            map.on('click', (e) => {
                const longtitude = e.lngLat.lng
                const lattitude = e.lngLat.lat

                @this.long = longtitude
                @this.lat = lattitude
            })
        })
    </script>
@endpush
