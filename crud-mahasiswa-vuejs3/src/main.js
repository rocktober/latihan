import { createApp } from "vue";
import App from "./App.vue";

import MenuButton from "./components/UI/MenuButton.vue";
import TitlePage from "./components/UI/TitlePage.vue";
import ThTable from "./components/UI/ThTable.vue";
import TdTable from "./components/UI/TdTable.vue";
import ConfirmDialog from "./components/UI/ConfirmDialog.vue";
import WarningMessage from "./components/UI/WarningMessage.vue";

const app = createApp(App);

app.config.unwrapInjectedRef = true;

app.component("menu-button", MenuButton);
app.component("title-page", TitlePage);
app.component("th-table", ThTable);
app.component("td-table", TdTable);
app.component("confirm-dialog", ConfirmDialog);
app.component("warning-message", WarningMessage);

app.mount("#app");
