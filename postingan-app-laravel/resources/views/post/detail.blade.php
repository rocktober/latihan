@extends('layouts.master')

@section('title')
Detail post
@endsection

@section('content')

<img src="{{asset('image/'. $post -> thumbnail)}}" class="card-img-top mb-4" alt="...">
<h3>{{ $post -> title }}</h3>
<span class="badge badge-success mb-2">{{ $post->category->name }}</span>
<p class="card-text">{{ $post -> content }}</p>
<a href="/post" class="btn btn-secondary btn-sm">Back</a>

<hr>
<h4>Comment</h4>

@forelse ($post->tag as $item)
<div class="media bg-secondary m-2">
    <div class="media-body m-2">
        <h5>{{ $item->user->name }}</h5>
        <p>{{ $item->name }}</p>
    </div>
</div>
@empty
<h1>No Comment</h1>
@endforelse

@auth
<form action="/tag" method="POST">
    @csrf
    <input type="hidden" name="post_id" value={{ $post->id }}>
    <textarea name="name" class="form-control" cols="30" rows="10"></textarea>
    <input type="submit" class="btn btn-primary mt-2" value="comment">
</form>
@endauth


@endsection