@extends('layouts.master')

@section('title')
List post
@endsection

@section('content')
@auth
<a href="/post/create" class="btn btn-primary btn-sm mb-4">Create Post</a>
@endauth

<div class="row">
    @forelse ($post as $item)
    <div class="col-lg-4">
        <div class="card">
            <img src="{{asset('image/'. $item -> thumbnail)}}" class="card-img-top rounded-top" alt="...">
            <div class="card-body text-center">
                <h3>{{ $item -> title }}</h3>
                <span class="badge badge-success mb-2">{{ $item->category->name }}</span>
                <p class="card-text">{{ Str::limit($item -> content, 100) }}</p>
                <a href="/post/{{ $item -> id }}" class="btn btn-secondary btn-sm btn-block">Read More</a>
                @auth
                <div class="row my-2">
                    <div class="col">
                        <a href="/post/{{ $item -> id }}/edit" class="btn btn-warning btn-sm btn-block">Edit</a>
                    </div>
                    <div class="col">
                        <form action="/post/{{ $item -> id }}" method="POST">
                            @csrf
                            @method('DELETE')
                            <input type="submit" class="btn btn-danger btn-sm btn-block" value="Delete"
                                onclick="confirmDelete()">
                        </form>
                    </div>
                </div>
                @endauth
            </div>
        </div>
    </div>
    @empty
    <h2>Tidak Ada Post</h2>
    @endforelse
</div>
@endsection