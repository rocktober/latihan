<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Post;
use File;
use RealRashid\SweetAlert\Facades\Alert;


class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $post = Post::all();
        return view('post.index', compact('post'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = Category::get();
        return view('post.create', compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'content' => 'required',
            'thumbnail' => 'required|image|mimes:png,jpg,jpeg',
            'category' => 'required',
        ]);

        $fileName = time() . '.' . $request->thumbnail->extension();
        $request->thumbnail->move(public_path('image'), $fileName);

        $post = new Post;
        $post->title = $request['title'];
        $post->content = $request['content'];
        $post->thumbnail = $fileName;
        $post->category_id = $request['category'];
        $post->save();

        Alert::success('Success', 'Success Add New Post');

        return redirect('/post');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::find($id);
        return view('post.detail', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::find($id);
        $category = Category::get();
        return view('post.edit', compact('post', 'category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'required',
            'content' => 'required',
            'thumbnail' => 'image|mimes:png,jpg,jpeg',
            'category' => 'required',
        ]);

        $post = Post::find($id);
        if ($request->has('thumbnail')) {
            $path = 'image/';

            File::delete($path . $post->thumbnail);

            $fileName = time() . '.' . $request->thumbnail->extension();
            $request->thumbnail->move(public_path('image'), $fileName);

            $post->thumbnail = $fileName;

            $post->save();
        }

        $post->title = $request['title'];
        $post->content = $request['content'];
        $post->category_id = $request['category'];
        $post->save();

        Alert::success('Success', 'Success Update Post');

        return redirect('/post');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::find($id);

        $path = 'image/';
        File::delete($path . $post->thumbnail);

        $post->delete();

        Alert::warning('Post Has Deleted');

        return redirect('/post');
    }
}
