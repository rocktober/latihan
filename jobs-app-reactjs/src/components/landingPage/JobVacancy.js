import React, { useContext, useEffect } from 'react';
import { GlobalContext } from '../../context/GlobalContext';
import { Link } from 'react-router-dom';
import image_not_found from '../../assets/image/image_not_found.png';

const JobVacancy = () => {
  const { state, handleFunction } = useContext(GlobalContext);
  const { display, fetchStatus, setFetchStatus, jobs, search } = state;
  const { fetchData, handleJobStatus, handleChangeSearch, handleSearch } = handleFunction;

  useEffect(() => {
    if (fetchStatus === true) {
      fetchData();
      setFetchStatus(false);
    }
  }, [fetchData, fetchStatus, setFetchStatus]);

  return (
    <>
      <div className="container p-10 text-center underline mx-auto">
        <h1 className="text-2xl font-bold">Job Vacancy</h1>
      </div>

      <div className="container grid grid-cols-1 mx-auto lg:grid-cols-4 w-full gap-6">
        <form onSubmit={handleSearch} className="lg:col-span-1 justify-center">
          <div className="lg:w-full mt-14 w-3/5 mx-auto">
            <div className="input-group flex w-full mb-4">
              <input
                value={search}
                onChange={handleChangeSearch}
                type="text"
                className="search form-control relative flex-auto min-w-0 block w-full px-3 py-1.5 text-base font-normal text-gray-700 bg-white bg-clip-padding border border-solid border-gray-300 rounded transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none"
                placeholder="Search"
                aria-label="Search"
                aria-describedby="button-addon2"
              />
              <button
                className="btn inline-block px-6 py-2.5 bg-blue-600 text-white font-medium text-xs leading-tight uppercase rounded shadow-md hover:bg-blue-700 hover:shadow-lg focus:bg-blue-700  focus:shadow-lg focus:outline-none focus:ring-0 active:bg-blue-800 active:shadow-lg transition duration-150 ease-in-out items-center"
                type="submit"
                id="button-addon2"
              >
                <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="search" className="w-4" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                  <path
                    fill="currentColor"
                    d="M505 442.7L405.3 343c-4.5-4.5-10.6-7-17-7H372c27.6-35.3 44-79.7 44-128C416 93.1 322.9 0 208 0S0 93.1 0 208s93.1 208 208 208c48.3 0 92.7-16.4 128-44v16.3c0 6.4 2.5 12.5 7 17l99.7 99.7c9.4 9.4 24.6 9.4 33.9 0l28.3-28.3c9.4-9.4 9.4-24.6.1-34zM208 336c-70.7 0-128-57.2-128-128 0-70.7 57.2-128 128-128 70.7 0 128 57.2 128 128 0 70.7-57.2 128-128 128z"
                  />
                </svg>
              </button>
            </div>
          </div>
        </form>

        {display && (
          <div className=" mx-auto grid grid-cols-1 lg:grid-cols-2 lg:col-span-3 gap-10 h-fit mb-20 p-5">
            <div role="status" className="space-y-8 animate-pulse md:space-y-0 md:space-x-8 md:flex md:items-center">
              <div className="flex justify-center items-center w-full h-48 bg-gray-300 rounded sm:w-96 dark:bg-gray-700">
                <svg className="w-12 h-12 text-gray-200" xmlns="http://www.w3.org/2000/svg" aria-hidden="true" fill="currentColor" viewBox="0 0 640 512">
                  <path d="M480 80C480 35.82 515.8 0 560 0C604.2 0 640 35.82 640 80C640 124.2 604.2 160 560 160C515.8 160 480 124.2 480 80zM0 456.1C0 445.6 2.964 435.3 8.551 426.4L225.3 81.01C231.9 70.42 243.5 64 256 64C268.5 64 280.1 70.42 286.8 81.01L412.7 281.7L460.9 202.7C464.1 196.1 472.2 192 480 192C487.8 192 495 196.1 499.1 202.7L631.1 419.1C636.9 428.6 640 439.7 640 450.9C640 484.6 612.6 512 578.9 512H55.91C25.03 512 .0006 486.1 .0006 456.1L0 456.1z" />
                </svg>
              </div>
              <div className="w-full">
                <div className="h-2.5 bg-gray-200 rounded-full dark:bg-gray-700 w-48 mb-4" />
                <div className="h-2 bg-gray-200 rounded-full dark:bg-gray-700 max-w-[480px] mb-2.5" />
                <div className="h-2 bg-gray-200 rounded-full dark:bg-gray-700 mb-2.5" />
                <div className="h-2 bg-gray-200 rounded-full dark:bg-gray-700 max-w-[440px] mb-2.5" />
                <div className="h-2 bg-gray-200 rounded-full dark:bg-gray-700 max-w-[460px] mb-2.5" />
                <div className="h-2 bg-gray-200 rounded-full dark:bg-gray-700 max-w-[360px]" />
              </div>
              <span className="sr-only">Loading...</span>
            </div>
            <div role="status" className="space-y-8 animate-pulse md:space-y-0 md:space-x-8 md:flex md:items-center">
              <div className="flex justify-center items-center w-full h-48 bg-gray-300 rounded sm:w-96 dark:bg-gray-700">
                <svg className="w-12 h-12 text-gray-200" xmlns="http://www.w3.org/2000/svg" aria-hidden="true" fill="currentColor" viewBox="0 0 640 512">
                  <path d="M480 80C480 35.82 515.8 0 560 0C604.2 0 640 35.82 640 80C640 124.2 604.2 160 560 160C515.8 160 480 124.2 480 80zM0 456.1C0 445.6 2.964 435.3 8.551 426.4L225.3 81.01C231.9 70.42 243.5 64 256 64C268.5 64 280.1 70.42 286.8 81.01L412.7 281.7L460.9 202.7C464.1 196.1 472.2 192 480 192C487.8 192 495 196.1 499.1 202.7L631.1 419.1C636.9 428.6 640 439.7 640 450.9C640 484.6 612.6 512 578.9 512H55.91C25.03 512 .0006 486.1 .0006 456.1L0 456.1z" />
                </svg>
              </div>
              <div className="w-full">
                <div className="h-2.5 bg-gray-200 rounded-full dark:bg-gray-700 w-48 mb-4" />
                <div className="h-2 bg-gray-200 rounded-full dark:bg-gray-700 max-w-[480px] mb-2.5" />
                <div className="h-2 bg-gray-200 rounded-full dark:bg-gray-700 mb-2.5" />
                <div className="h-2 bg-gray-200 rounded-full dark:bg-gray-700 max-w-[440px] mb-2.5" />
                <div className="h-2 bg-gray-200 rounded-full dark:bg-gray-700 max-w-[460px] mb-2.5" />
                <div className="h-2 bg-gray-200 rounded-full dark:bg-gray-700 max-w-[360px]" />
              </div>
              <span className="sr-only">Loading...</span>
            </div>
          </div>
        )}

        <div className="mx-auto grid grid-cols-1 lg:grid-cols-2 lg:col-span-3 gap-6 h-fit mb-20 p-5">
          {jobs !== null &&
            jobs.map((res, index) => {
              return (
                <div key={index} className="overflow-hidden shadow-lg rounded-lg">
                  <Link to={`/job-vacancy/${res.id}`} className="flex flex-col items-center bg-white md:flex-row md:max-w-xl hover:scale-105 transition duration-400 p-4">
                    {res.company_image_url === null ? (
                      <img className="object-cover w-full h-96 rounded-lg md:h-auto md:w-48 md:rounded-lg" src={image_not_found} alt="" />
                    ) : (
                      <img className="object-cover w-full h-96 rounded-lg md:h-auto md:w-48 md:rounded-lg" src={res.company_image_url} alt="" />
                    )}
                    <div className="flex flex-col justify-between p-4 leading-normal">
                      <h2 className="mb-2 text-xl font-bold tracking-tight text-slate-900 dark:text-gray-100">
                        {res.title} ({res.job_type})
                      </h2>
                      <p className="font-normal text-gray-700 dark:text-gray-400">{res.company_name}</p>
                      <p className="mb-2 font-light text-sm text-gray-700 dark:text-gray-400">{res.company_city}</p>
                      <p className="mb-2 font-light text-sm text-gray-700 dark:text-gray-400">{res.job_tenure}</p>
                      <span className="text-sm text-gray-700 dark:text-gray-500">{handleJobStatus(res.job_status)}</span>
                    </div>
                  </Link>
                </div>
              );
            })}
        </div>
      </div>
    </>
  );
};

export default JobVacancy;
