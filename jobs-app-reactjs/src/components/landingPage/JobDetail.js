import React, { useContext, useEffect } from 'react';
import { GlobalContext } from '../../context/GlobalContext';
import { useParams } from 'react-router-dom';
import axios from 'axios';
import image_not_found from '../../assets/image/image_not_found.png';

const JobDetail = () => {
  let { id } = useParams();
  const { state, handleFunction } = useContext(GlobalContext);
  const { jobs, setJobs } = state;
  const { handleToRupiah } = handleFunction;

  useEffect(() => {
    if (id !== undefined) {
      axios
        .get(`https://dev-example.sanbercloud.com/api/job-vacancy/${id}`)
        .then((res) => {
          setJobs(res.data);
        })
        .catch((error) => {
          console.log(error);
        });
    }
  }, [id, setJobs]);

  return (
    <>
      <div className="container p-2 text-center underline mx-auto mt-10">
        <h1 className="text-2xl font-bold">Detail Job</h1>
      </div>
      <section className="dark:bg-gradient-to-tr dark:from-gray-900 dark:to-slate-800 min-h-screen relative pb-20">
        <div className="py-4 px-6 sm:py-5 sm:px-20 md:px-44 dark:text-gray-100 min-h-fit">
          {jobs !== null && (
            <div className="pt-10 flex flex-col md:flex-row justify-center items-start gap-10">
              <div className="w-full md:w-4/12 lg:w-3/12">{jobs.company_image_url === null ? <img className="w-96" src={image_not_found} alt="" /> : <img className="w-96" src={jobs.company_image_url} alt="" />}</div>
              <div className="w-full bg-gray-100 md:8/12 lg:w-9/12 shadow-lg py-3 px-3 dark:bg-slate-800 rounded-md">
                <div className="mb-2">
                  <p className="text-3xl font-medium">
                    {jobs.title} ({jobs.job_tenure})
                  </p>
                </div>
                <div className="mb-2 flex items-center gap-x-10">
                  <p className="text-2xl font-medium">
                    {jobs.company_name} - {jobs.job_type}
                  </p>
                </div>
                <div className="flex mb-6">
                  <svg className="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M17.657 16.657L13.414 20.9a1.998 1.998 0 01-2.827 0l-4.244-4.243a8 8 0 1111.314 0z" />
                    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M15 11a3 3 0 11-6 0 3 3 0 016 0z" />
                  </svg>
                  <p className="text-md font-light ml-2">{jobs.company_city}</p>
                </div>
                <div className="mb-4 border-b pb-2">
                  <h5 className="text-lg">Description</h5>
                  <p className="font-light text-md">{jobs.job_description}</p>
                </div>
                <div className="mb-4 border-b pb-2">
                  <h5 className="text-lg">Qualification</h5>
                  <p className="font-light text-md">{jobs.job_qualification}</p>
                </div>
                <div className="mb-6">
                  <h5 className="text-lg">Salary</h5>
                  <span className="text-md">
                    {handleToRupiah(jobs.salary_min)} - {handleToRupiah(jobs.salary_max)}
                  </span>
                </div>
                <a href="/job-vacancy" className="flex items-center tracking-wider justify-center text-sm sm:text-md text-white rounded-md transition duration-200 bg-indigo-500 hover:bg-indigo-400 py-1 px-3 w-fit">
                  <svg className="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M11 19l-7-7 7-7m8 14l-7-7 7-7" />
                  </svg>
                  Back
                </a>
              </div>
            </div>
          )}
        </div>
      </section>
    </>
  );
};

export default JobDetail;
