import React from 'react';
import { Navbar, Button } from 'flowbite-react';

const NavbarLanding = () => {
  return (
    <>
      <Navbar fluid={true} rounded={true}>
        <Navbar.Brand href="https://flowbite.com/">
          <span className="self-center whitespace-nowrap text-xl font-semibold dark:text-white">DENTA</span>
        </Navbar.Brand>
        <div className="flex md:order-2">
          <Button>Login</Button>
          <Navbar.Toggle />
        </div>
        <Navbar.Collapse>
          <Navbar.Link href="/">Home</Navbar.Link>
          <Navbar.Link href="/job-vacancy">Job Vacancy</Navbar.Link>
        </Navbar.Collapse>
      </Navbar>
    </>
  );
};

export default NavbarLanding;
