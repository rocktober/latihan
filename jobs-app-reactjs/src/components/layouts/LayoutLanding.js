import React from 'react';
import FooterLanding from '../footer/FooterLanding';
import NavbarLanding from '../navbar/NavbarLanding';

const LayoutLanding = (props) => {
  return (
    <>
      <NavbarLanding />
      {props.children}
      <FooterLanding />
    </>
  );
};

export default LayoutLanding;
