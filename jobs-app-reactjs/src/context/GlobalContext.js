import axios from 'axios';
import React, { createContext, useState } from 'react';

export const GlobalContext = createContext();

export const GlobalProvider = (props) => {
  // Untuk menampilkan skeleton sebelum data muncul
  const [display, setDisplay] = useState(true);

  // Indikator
  const [fetchStatus, setFetchStatus] = useState(true);

  // Input search
  const [search, setSearch] = useState('');

  const handleChangeSearch = (e) => {
    setSearch(e.target.value);
  };
  const handleSearch = (e) => {
    e.preventDefault();
    let searchData = jobs.filter((res) => {
      return Object.values(res).join(' ').toLowerCase().includes(search.toLowerCase());
    });
    setJobs([...searchData]);
    setSearch('');
  };

  // Fetch Data
  const [jobs, setJobs] = useState([]);
  let fetchData = () => {
    setDisplay(true);
    axios
      .get('https://dev-example.sanbercloud.com/api/job-vacancy')
      .then((res) => {
        setJobs([...res.data.data]);
        setDisplay(false);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  // Format job status
  const handleJobStatus = (param) => {
    if (param === 1) {
      return 'Open';
    } else if (param === 0) {
      return 'Close';
    }
  };

  // Format Rupiah
  const handleToRupiah = (angka) => {
    let rupiah = '';

    if (angka === undefined) {
      return '0';
    } else {
      let angkarev = angka.toString().split('').reverse().join('');
      for (var i = 0; i < angkarev.length; i++) if (i % 3 === 0) rupiah += angkarev.substr(i, 3) + '.';
    }
    return (
      'Rp. ' +
      rupiah
        .split('', rupiah.length - 1)
        .reverse()
        .join('')
    );
  };

  // Initial State dan Function
  let state = {
    display,
    setDisplay,
    fetchStatus,
    setFetchStatus,
    jobs,
    setJobs,
    search,
    setSearch,
  };

  let handleFunction = {
    fetchData,
    handleJobStatus,
    handleToRupiah,
    handleChangeSearch,
    handleSearch,
  };

  return (
    <GlobalContext.Provider
      value={{
        state,
        handleFunction,
      }}
    >
      {props.children}
    </GlobalContext.Provider>
  );
};
