import React from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Home from './components/landingPage/Home';
import JobDetail from './components/landingPage/JobDetail';
import JobVacancy from './components/landingPage/JobVacancy';
import LayoutLanding from './components/layouts/LayoutLanding';
import { GlobalProvider } from './context/GlobalContext';

const App = () => {
  return (
    <>
      <BrowserRouter>
        <GlobalProvider>
          <Routes>
            <Route
              path="/"
              element={
                <LayoutLanding>
                  <Home />
                </LayoutLanding>
              }
            />

            <Route
              path="/job-vacancy"
              element={
                <LayoutLanding>
                  <JobVacancy />
                </LayoutLanding>
              }
            />

            <Route
              path="/job-vacancy/:id"
              element={
                <LayoutLanding>
                  <JobDetail />
                </LayoutLanding>
              }
            />
          </Routes>
        </GlobalProvider>
      </BrowserRouter>
    </>
  );
};

export default App;
