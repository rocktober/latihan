import { BrowserRouter, Routes, Route } from 'react-router-dom';
import './App.css';
import TodoDashboard from './components/TodoDashboard';
import { GlobalProvider } from './context/GlobalContext';
import Layout from './components/layouts/Layout';
import TodoDetail from './components/TodoDetail';
function App() {
  return (
    <>
      <BrowserRouter>
        <GlobalProvider>
          <Routes>
            <Route
              path="/"
              element={
                <Layout>
                  <TodoDashboard />
                </Layout>
              }
            />

            <Route
              path="/detail/:id"
              element={
                <Layout>
                  <TodoDetail />
                </Layout>
              }
            />
          </Routes>
        </GlobalProvider>
      </BrowserRouter>
    </>
  );
}

export default App;
