import React, { createContext, useState } from 'react';
import axios from 'axios';

export const GlobalContext = createContext();

export const GlobalProvider = (props) => {
  const [fetchStatus, setFetchStatus] = useState(true);
  // Get All Todo
  const [todo, setTodo] = useState(null);
  const [input, setInput] = useState({ title: '' });
  const [updateData, setUpdateData] = useState();

  const fetchData = () => {
    axios.get('https://floating-mountain-35184.herokuapp.com/activity-groups').then((res) => {
      //   console.log([...res.data.data]);
      setTodo([...res.data.data]);
    });
  };

  // Create Todo
  const addTodo = () => {
    let newEntry = axios.post('https://floating-mountain-35184.herokuapp.com/activity-groups', { email: 'peserta@gmail.com', title: 'New Activity' }).then((res) => {
      //   console.log(res);
      setTodo([res, newEntry]);
      setFetchStatus(true);
    });
  };

  // Delete Todo
  const deleteTodo = (id) => {
    axios.delete(`https://floating-mountain-35184.herokuapp.com/activity-groups/${id}`);
    let todoDelete = todo.filter((todo) => todo.id !== id);
    setTodo(todoDelete);
  };

  // Update Todo
  const todoUpdate = (id) => {
    axios.put(`https://floating-mountain-35184.herokuapp.com/activity-groups/${id}`, { title: 'New' }).then((res) => {
      console.log(res);
    });
  };

  const changeTodo = (e) => {
    let { name, value } = todo;
    setInput({ ...todo, [name]: value });
    // console.log(setInput);
  };

  let state = {
    fetchStatus,
    setFetchStatus,
    todo,
    setTodo,
  };

  let handleFunction = {
    fetchData,
    addTodo,
    deleteTodo,
    todoUpdate,
    changeTodo,
  };

  return (
    <GlobalContext.Provider
      value={{
        state,
        handleFunction,
      }}
    >
      {props.children}
    </GlobalContext.Provider>
  );
};
