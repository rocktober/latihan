import React, { useContext, useEffect } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTrashCan, faPlus } from '@fortawesome/free-solid-svg-icons';
import { GlobalContext } from '../context/GlobalContext';
import { Link } from 'react-router-dom';

const TodoDashboard = () => {
  const { state, handleFunction } = useContext(GlobalContext);

  const { fetchStatus, setFetchStatus, todo } = state;

  const { fetchData, addTodo, deleteTodo } = handleFunction;

  useEffect(() => {
    if (fetchStatus === true) {
      fetchData();
      setFetchStatus(false);
    }
  }, [fetchData, fetchStatus, setFetchStatus]);

  return (
    <div className="container">
      <div className="todo-header">
        <h1 data-cy="activity-title">Activity</h1>
        <button data-cy="activity-add-button" className="btn" onClick={addTodo}>
          <FontAwesomeIcon className="icon-plus" icon={faPlus} />
          Tambah
        </button>
      </div>
      <div className="dashboard-content">
        <div className="row">
          {todo !== null &&
            todo.map((res, index) => {
              return (
                <div key={index} className="col-3">
                  <div data-cy="activity-item" className="activity-card">
                    <Link to={`detail/${res.id}`} className="activity-body">
                      <h4 data-cy="activity-item-title">{res.title}</h4>
                    </Link>
                    <div className="card-footer">
                      <span data-cy="activity-item-date">{res.created_at}</span>
                      <span onClick={() => deleteTodo(res.id)} className="icon-trash">
                        <FontAwesomeIcon icon={faTrashCan} />
                      </span>
                    </div>
                  </div>
                </div>
              );
            })}
        </div>
      </div>
    </div>
  );
};

export default TodoDashboard;
