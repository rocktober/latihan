import React from 'react';
import Navbar from '../Navbar';

const layout = (props) => {
  return (
    <>
      <Navbar />
      {props.children}
    </>
  );
};

export default layout;
