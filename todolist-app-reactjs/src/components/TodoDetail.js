import React, { useContext, useEffect } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faChevronLeft, faPlus, faPen, faArrowDownWideShort, faArrowUpShortWide, faArrowDownAZ, faArrowDownZA } from '@fortawesome/free-solid-svg-icons';
import { GlobalContext } from '../context/GlobalContext';
import { useParams } from 'react-router-dom';
import axios from 'axios';

const TodoDetail = () => {
  let { id } = useParams();

  const { state, handleFunction } = useContext(GlobalContext);

  const { todo, setTodo } = state;

  const { changeTodo, todoUpdate } = handleFunction;

  useEffect(() => {
    if (id !== undefined) {
      axios.get(`https://floating-mountain-35184.herokuapp.com/activity-groups/${id}`).then((res) => {
        setTodo(res.data.data);
      });
    }
  }, [id, setTodo]);

  return (
    <div className="container">
      <div className="todo-header">
        <div className="todo-title">
          <a href="/" data-cy="todo-back-button" className="icon-back">
            <FontAwesomeIcon className="icon-arrow" icon={faChevronLeft} />
          </a>
          {todo !== null && (
            <>
              <h1 data-cy="todo-title">{todo.title}</h1>
              <button data-cy="todo-title-edit-button" className="icon-edit" onClick={changeTodo} value={todo.title}>
                <FontAwesomeIcon className="icon-pen" icon={faPen} />
              </button>
            </>
          )}
        </div>
        <div className="d-flex">
          <div className="dropdown">
            <div>
              <button data-cy="todo-sort-button" className="btn-sort" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                <i className="bi bi-arrow-down-up"></i>
              </button>
              <ul className="dropdown-menu">
                <a data-cy="sort-selection" className="dropdown-item" href="/">
                  <div data-cy="false" className="d-flex item-label false">
                    <FontAwesomeIcon data-cy="sort-selection-icon" className="icon-selection" icon={faArrowDownWideShort} />
                    <span data-cy="sort-selection-title">Terbaru</span>
                  </div>
                </a>

                <a data-cy="sort-selection" className="dropdown-item" href="/">
                  <div data-cy="false" className="d-flex item-label false">
                    <FontAwesomeIcon data-cy="sort-selection-icon" className="icon-selection" icon={faArrowUpShortWide} />
                    <span data-cy="sort-selection-title">Terlama</span>
                  </div>
                </a>

                <a data-cy="sort-selection" className="dropdown-item" href="/">
                  <div data-cy="false" className="d-flex item-label false">
                    <FontAwesomeIcon data-cy="sort-selection-icon" className="icon-selection" icon={faArrowDownAZ} />
                    <span data-cy="sort-selection-title">A-Z</span>
                  </div>
                </a>

                <a data-cy="sort-selection" className="dropdown-item" href="/">
                  <div data-cy="false" className="d-flex item-label false">
                    <FontAwesomeIcon data-cy="sort-selection-icon" className="icon-selection" icon={faArrowDownZA} />
                    <span data-cy="sort-selection-title">Z-A</span>
                  </div>
                </a>

                <a data-cy="sort-selection" className="dropdown-item" href="/">
                  <div data-cy="false" className="d-flex item-label false">
                    <i data-cy="sort-selection-icon" className="bi bi-arrow-down-up icon-selection"></i>
                    <span data-cy="sort-selection-title">Belum Selesai</span>
                  </div>
                </a>
              </ul>
            </div>
          </div>
          <button data-cy="activity-add-button" className="btn">
            <FontAwesomeIcon className="icon-plus" icon={faPlus} />
            Tambah
          </button>
        </div>
      </div>
    </div>
  );
};

export default TodoDetail;
