import React from 'react';

const Navbar = () => {
  return (
    <div className="header">
      <div data-cy="header-background" className="container">
        <h2 data-cy="header-title">TO DO LIST APP</h2>
      </div>
    </div>
  );
};

export default Navbar;
