@extends('layouts.admin')

@section('content')
<div class="card">
    <div class="card-header">
        <h4>Create Product</h4>
    </div>
    <div class="card-body">
        <form action="/products" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-md-12 mb-3">
                    <select class="form-select" name="category_id">
                        <option value="">Select a Category</option>
                        @foreach ($category as $item)
                        <option value="{{ $item->id }}">{{ $item->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-6 mb-3">
                    <div class="input-group input-group-outline">
                        <label for="name" class="form-label">Name</label>
                        <input type="text" class="form-control" name="name">
                    </div>
                </div>
                <div class="col-md-6 mb-3">
                    <div class="input-group input-group-outline">
                        <label for="slug" class="form-label">Slug</label>
                        <input type="text" class="form-control" name="slug">
                    </div>
                </div>
                <div class="col-md-12 mb-3">
                    <div class="input-group input-group-outline">
                        <label for="description" class="form-label"></label>
                        <textarea name="description" rows="3" class="form-control" placeholder="Description"></textarea>
                    </div>
                </div>
                <div class="col-md-6 mb-3">
                    <div class="input-group input-group-outline">
                        <label for="original_price" class="form-label">Original Price</label>
                        <input type="number" name="original_price" class="form-control">
                    </div>
                </div>
                <div class="col-md-6 mb-3">
                    <div class="input-group input-group-outline">
                        <label for="selling_price" class="form-label">Selling Price</label>
                        <input type="number" name="selling_price" class="form-control">
                    </div>
                </div>
                <div class="col-md-6 mb-3">
                    <div class="input-group input-group-outline">
                        <label for="tax" class="form-label">Tax</label>
                        <input type="number" name="tax" class="form-control">
                    </div>
                </div>
                <div class="col-md-6 mb-3">
                    <div class="input-group input-group-outline">
                        <label for="qty" class="form-label">Quantity</label>
                        <input type="number" name="qty" class="form-control">
                    </div>
                </div>
                <div class="col-md-6 mb-3 form-check">
                    <label for="status">Status</label>
                    <input type="checkbox" name="status" class="form-check-input">
                </div>
                <div class="col-md-6 mb-3 form-check">
                    <label for="trending">Trending</label>
                    <input type="checkbox" name="trending" class="form-check-input">
                </div>
                <div class="col-md-6 mb-3">
                    <div class="input-group input-group-outline">
                        <label for="meta_title" class="form-label">Meta Title</label>
                        <input type="text" class="form-control" name="meta_title">
                    </div>
                </div>
                <div class="col-md-12 mb-3">
                    <div class="input-group input-group-outline">
                        <label for="meta_description" class="form-label"></label>
                        <textarea name="meta_description" rows="3" class="form-control"
                            placeholder="Meta Description"></textarea>
                    </div>
                </div>
                <div class="col-md-12 mb-3">
                    <div class="input-group input-group-outline">
                        <label for="meta_keywords" class="form-label"></label>
                        <textarea name="meta_keywords" rows="3" class="form-control"
                            placeholder="Meta Keywords"></textarea>
                    </div>
                </div>
                <div class="col-md-12 mb-3">
                    <input type="file" name="image">
                </div>
                <div class="col-md-12">
                    <button type="submit" class="btn btn-info">Create</button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection