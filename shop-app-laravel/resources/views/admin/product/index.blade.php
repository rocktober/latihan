@extends('layouts.admin')

@section('content')
<div class="row">
    <div class="col-12">
        <a href="/products/create" class="btn btn-info btn-sm">Create</a>
        <div class="card my-4">
            <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                <div class="bg-gradient-success shadow-primary border-radius-lg pt-4 pb-3">
                    <h5 class="text-white text-capitalize ps-3">Products Table</h5>
                </div>
            </div>
            <div class="card-body px-0 pb-2">
                <div class="table-responsive p-0">
                    <table class="table align-items-center mb-0 text-center">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Category</th>
                                <th>Name</th>
                                <th>Selling</th>
                                <th>Image</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($product as $key => $item)
                            <tr>
                                <td>{{ $key + 1 }}</td>
                                <td>{{ $item->category->name }}</td>
                                <td>{{ $item->name }}</td>
                                <td>{{ $item->selling_price }}</td>
                                <td>
                                    <img src="{{ asset('assets/uploads/products/'. $item->image) }}"
                                        class="product-image" alt="Image here">
                                </td>
                                <td>
                                    <form action="/products/{{ $item->id }}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <a href="/products/{{ $item->id }}/edit" class="btn btn-warning btn-sm">Edit</a>
                                        <button type="submit" value="delete"
                                            class="btn btn-danger btn-sm">Delete</button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection