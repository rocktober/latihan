@extends('layouts.admin')

@section('content')
<div class="card">
    <div class="card-header">
        <h4>Create Product</h4>
    </div>
    <div class="card-body">
        <form action="/products/{{ $product->id }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="row">
                <div class="col-md-12 mb-3">
                    <select class="form-select" name="category_id">
                        <option value="">{{ $product->category->name }}</option>
                    </select>
                </div>
                <div class="col-md-6 mb-3">
                    <div class="input-group input-group-outline">
                        <label for="name" class="form-label">Name</label>
                        <input type="text" class="form-control" value="{{ $product->name }}" name="name">
                    </div>
                </div>
                <div class="col-md-6 mb-3">
                    <div class="input-group input-group-outline">
                        <label for="slug" class="form-label">Slug</label>
                        <input type="text" class="form-control" value="{{ $product->slug }}" name="slug">
                    </div>
                </div>
                <div class="col-md-12 mb-3">
                    <div class="input-group input-group-outline">
                        <label for="description" class="form-label"></label>
                        <textarea name="description" rows="3" class="form-control"
                            placeholder="Description">{{ $product->description }}</textarea>
                    </div>
                </div>
                <div class="col-md-6 mb-3">
                    <div class="input-group input-group-outline">
                        <label for="original_price" class="form-label">Original Price</label>
                        <input type="number" name="original_price" value="{{ $product->original_price }}"
                            class="form-control">
                    </div>
                </div>
                <div class="col-md-6 mb-3">
                    <div class="input-group input-group-outline">
                        <label for="selling_price" class="form-label">Selling Price</label>
                        <input type="number" name="selling_price" value="{{ $product->selling_price }}"
                            class="form-control">
                    </div>
                </div>
                <div class="col-md-6 mb-3">
                    <div class="input-group input-group-outline">
                        <label for="tax" class="form-label">Tax</label>
                        <input type="number" name="tax" value="{{ $product->tax }}" class="form-control">
                    </div>
                </div>
                <div class="col-md-6 mb-3">
                    <div class="input-group input-group-outline">
                        <label for="qty" class="form-label">Quantity</label>
                        <input type="number" name="qty" value="{{ $product->qty }}" class="form-control">
                    </div>
                </div>
                <div class="col-md-6 mb-3 form-check">
                    <label for="status">Status</label>
                    <input type="checkbox" {{ $product->status == "1" ? 'checked' : '' }} name="status"
                    class="form-check-input">
                </div>
                <div class="col-md-6 mb-3 form-check">
                    <label for="trending">Trending</label>
                    <input type="checkbox" {{ $product->trending == "1" ? 'checked' : '' }} name="trending"
                    class="form-check-input">
                </div>
                <div class="col-md-6 mb-3">
                    <div class="input-group input-group-outline">
                        <label for="meta_title" class="form-label">Meta Title</label>
                        <input type="text" class="form-control" value="{{ $product->meta_title }}" name="meta_title">
                    </div>
                </div>
                <div class="col-md-12 mb-3">
                    <div class="input-group input-group-outline">
                        <label for="meta_description" class="form-label"></label>
                        <textarea name="meta_description" rows="3" class="form-control"
                            placeholder="Meta Description">{{ $product->meta_description }}</textarea>
                    </div>
                </div>
                <div class="col-md-12 mb-3">
                    <div class="input-group input-group-outline">
                        <label for="meta_keywords" class="form-label"></label>
                        <textarea name="meta_keywords" rows="3" class="form-control"
                            placeholder="Meta Keywords">{{ $product->meta_keywords }}</textarea>
                    </div>
                </div>
                @if ($product->image)
                <img src="{{ asset('assets/uploads/products/'. $product->image) }}" class="product-image"
                    alt="Product image">
                @endif
                <div class="col-md-12 mb-3">
                    <input type="file" name="image">
                </div>
                <div class="col-md-12">
                    <button type="submit" class="btn btn-info">Update</button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection