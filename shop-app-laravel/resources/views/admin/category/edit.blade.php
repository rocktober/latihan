@extends('layouts.admin')

@section('content')
<div class="card">
    <div class="card-header">
        <h4>Edit Category</h4>
    </div>
    <div class="card-body">
        <form action="/categories/{{ $category->id }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="row">
                <div class="col-md-6 mb-3">
                    <div class="input-group input-group-outline">
                        <label for="name" class="form-label">Name</label>
                        <input type="text" value="{{ $category->name }}" class="form-control" name="name">
                    </div>
                </div>
                <div class="col-md-6 mb-3">
                    <div class="input-group input-group-outline">
                        <label for="slug" class="form-label">Slug</label>
                        <input type="text" value="{{ $category->slug }}" class="form-control" name="slug">
                    </div>
                </div>
                <div class="col-md-12 mb-3">
                    <div class="input-group input-group-outline">
                        <label for="description" class="form-label"></label>
                        <textarea name="description" rows="3" class="form-control"
                            placeholder="Description">{{ $category->description }}</textarea>
                    </div>
                </div>
                <div class="col-md-6 mb-3 form-check">
                    <label for="status">Status</label>
                    <input type="checkbox" {{ $category->status == "1" ? 'checked' : '' }} name="status"
                    class="form-check-input">
                </div>
                <div class="col-md-6 mb-3 form-check">
                    <label for="popular">Popular</label>
                    <input type="checkbox" {{ $category->popular == "1" ? 'checked' : '' }} name="popular"
                    class="form-check-input">
                </div>
                <div class="col-md-6 mb-3">
                    <div class="input-group input-group-outline">
                        <label for="meta_title" class="form-label">Meta Title</label>
                        <input type="text" value="{{ $category->meta_title }}" class="form-control" name="meta_title">
                    </div>
                </div>
                <div class="col-md-12 mb-3">
                    <div class="input-group input-group-outline">
                        <label for="meta_description" class="form-label"></label>
                        <textarea name="meta_description" rows="3" class="form-control"
                            placeholder="Meta Description">{{ $category->meta_description }}</textarea>
                    </div>
                </div>
                <div class="col-md-12 mb-3">
                    <div class="input-group input-group-outline">
                        <label for="meta_keywords" class="form-label"></label>
                        <textarea name="meta_keywords" rows="3" class="form-control"
                            placeholder="Meta Keywords">{{ $category->meta_keywords }}</textarea>
                    </div>
                </div>
                @if ($category->image)
                <img src="{{ asset('assets/uploads/categories/'. $category->image) }}" class="category-image"
                    alt="Category image">
                @endif
                <div class="col-md-12 mb-3">
                    <input type="file" name="image">
                </div>
                <div class="col-md-12">
                    <button type="submit" class="btn btn-info">Update</button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection