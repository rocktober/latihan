@extends('layouts.front')

@section('title')
{{ $product->name }}
@endsection

@section('content')
<div class="py-3 mb-4 shadow-sm bg-warning border-top">
    <div class="container">
        <h6 class="mb-0">
            <a href="/category">
                Category
            </a> /
            <a href="/category/{{ $product->category->slug }}">
                {{ $product->category->name }}
            </a> /
            <a>
                {{ $product->name }}
            </a>
        </h6>
    </div>
</div>

<div class="container">
    <div class="card shadow product_data">
        <div class="card-body">
            <div class="row">
                <div class="col-md-4 border-right">
                    <img src="{{ asset('assets/uploads/products/'. $product->image) }}" class="w-100" alt="">
                </div>
                <div class="col-md-8">
                    <h2 class="mb-0">
                        {{ $product->name }}
                        @if($product->trending == '1')
                        <label for="trending" style="font-size: 16px"
                            class="float-end badge bg-danger trending_tag">Trending</label>
                        @endif
                    </h2>

                    <hr>
                    <label for="original_price" class="me-3">Original Price : <s>Rp {{ $product->original_price
                            }}</s></label>
                    <label for="selling_price" class="fw-bold">Selling Price : Rp {{ $product->selling_price }}</label>
                    <p class="mt-3">
                        {{ $product->description }}
                    </p>
                    <hr>
                    @if($product->qty > 0)
                    <label for="in_stock" class="badge bg-success">In stock</label>
                    @else
                    <label for="out_of_stock" class="badge bg-danger">Out of stock</label>
                    @endif
                    <div class="row mt-2">
                        <div class="col-md-3">
                            <input type="hidden" value="{{ $product->id }}" class="product_id">
                            <label for="quantity">Quantity</label>
                            <div class="input-group text-center mb-3" style="width: 130px">
                                <button class="input-group-text decrement-btn">-</button>
                                <input type="text" name="quantity" value='1' class="form-control text-center qty-input">
                                <button class="input-group-text increment-btn">+</button>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <br />
                            <button type="button" class="btn btn-primary addToCart me-3 float-start">Add to Cart <i
                                    class="fa fa-shopping-cart"></i></button>
                            <button type="button" class="btn btn-success me-3 float-start">Add to Wishlist <i
                                    class="fa fa-heart"></i></button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <hr>
                <h3>Description</h3>
                <p class="mt-3">
                    {!! $product->description !!}
                </p>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script>
    $(document).ready(function () {
        $('.addToCart').click(function (e) {
            e.preventDefault();

            var prod_id = $(this).closest('.product_data').find('.product_id').val();
            var prod_qty = $(this).closest('.product_data').find('.qty-input').val();

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                method: "POST",
                url: "/add-to-cart",
                data: {
                    'product_id': prod_id,
                    'product_qty': prod_qty
                },
                success: function (response) {
                    swal(response.status);
                }
            })
        })

        $('.increment-btn').click(function (e) {
            e.preventDefault();

            var inc_value = $('.qty-input').val();
            var value = parseInt(inc_value, 10);
            value = isNaN(value) ? 0 : value;
            if(value < 10)
            {
                value++;
                $('.qty-input').val(value);
            }
        });

        $('.decrement-btn').click(function (e) {
            e.preventDefault();

            var inc_value = $('.qty-input').val();
            var value = parseInt(inc_value, 10);
            value = isNaN(value) ? 0 : value;
            if(value > 1)
            {
                value--;
                $('.qty-input').val(value);
            }
        });
    });
</script>
@endsection