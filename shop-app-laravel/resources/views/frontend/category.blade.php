@extends('layouts.front')

@section('title')
Category
@endsection

@section('content')
<div class="py-5">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>All Categories</h2>
                <div class="row">
                    @foreach ($featured_category as $category)
                    <div class="col-md-3 mt-3">
                        <a href="/category/{{ $category->slug }}">
                            <div class="card card-item">
                                <img class="image-corousel"
                                    src="{{ asset('assets/uploads/categories/'. $category->image) }}"
                                    alt="category image">
                                <div class="card-body">
                                    <h5>{{ $category->name }}</h5>
                                    <p>{{ $category->description }}</p>
                                </div>
                            </div>
                        </a>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection