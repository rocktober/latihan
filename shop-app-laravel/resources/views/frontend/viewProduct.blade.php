@extends('layouts.front')

@section('title')
{{ $category->name }}
@endsection

@section('content')
<div class="py-3 mb-4 shadow-sm bg-warning border-top">
    <div class="container">
        <h6 class="mb-0">
            <a href="/category">
                Category
            </a> /
            <a>
                {{ $category->name }}
            </a>
        </h6>
    </div>
</div>

<div class="py-5">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>{{ $category->name }}</h2>
                <div class="row">
                    @foreach ($product as $item)
                    <div class="col-md-3 mt-3">
                        <a href="/category/{{ $category->slug }}/{{ $item->slug }}">
                            <div class="card card-item">
                                <img class="image-corousel" src="{{ asset('assets/uploads/products/'. $item->image) }}"
                                    alt="category image">
                                <div class="card-body">
                                    <h5>{{ $item->name }}</h5>
                                    <span class="float-start">Rp {{ $item->selling_price }}</span>
                                    <span class="float-end"><s>Rp {{ $item->original_price }}</s></span>
                                </div>
                            </div>
                        </a>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection