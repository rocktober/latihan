@extends('layouts.front')

@section('title')
Welcome to ADA.SHOP
@endsection

@section('content')
@include('layouts.partials.frontSlider')

<div class="py-5">
    <div class="container">
        <div class="row">
            <h2>Trending Products</h2>
            <div class="owl-carousel featured-carousel owl-theme">
                @foreach ($trending_product as $trending)
                <div class="item mt-3">
                    <div class="card">
                        <img class="image-corousel" src="{{ asset('assets/uploads/products/'. $trending->image) }}"
                            alt="product image">
                        <div class="card-body">
                            <h5>{{ $trending->name }}</h5>
                            <span class="float-start">{{ $trending->selling_price }}</span>
                            <span class="float-end"><s>{{ $trending->original_price }}</s></span>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</div>

<div class="py-5">
    <div class="container">
        <div class="row">
            <h2>Popular Category</h2>
            <div class="owl-carousel featured-carousel owl-theme">
                @foreach ($popular_category as $popular)
                <div class="item mt-3">
                    <a href="/view-category/{{ $popular->slug }}">
                        <div class="card card-item">
                            <img class="image-corousel" src="{{ asset('assets/uploads/categories/'. $popular->image) }}"
                                alt="product image">
                            <div class="card-body">
                                <h5>{{ $popular->name }}</h5>
                                <p>{{ $popular->description }}</p>
                            </div>
                        </div>
                    </a>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $('.featured-carousel').owlCarousel({
            loop:false,
            margin:10,
            nav:false,
            dots:true,
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:3
                },
                1024:{
                    items:4
                }
            }
        });
</script>
@endsection