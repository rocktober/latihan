<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Category;

class FrontendController extends Controller
{
    public function index()
    {
        $trending_product = Product::where('trending', '1')->take(15)->get();
        $popular_category = Category::where('popular', '1')->take(15)->get();
        return view('frontend.index', compact('trending_product', 'popular_category'));
    }

    public function category()
    {
        $featured_category = Category::where('status', '0')->get();
        return view('frontend.category', compact('featured_category'));
    }

    public function viewProduct($slug)
    {
        if (Category::where('slug', $slug)->exists()) {
            $category = Category::where('slug', $slug)->first();
            $product = Product::where('category_id', $category->id)->where('status', '0')->get();
            return view('frontend.viewProduct', compact('category', 'product'));
        } else {
            return redirect('/')->with('status', 'Category does not exsist');
        }
    }

    public function detailProduct($cate_slug, $prod_slug)
    {
        if (Category::where('slug', $cate_slug)->exists()) {
            if (Product::where('slug', $prod_slug)->exists()) {
                $product = Product::where('slug', $prod_slug)->first();
                return view('frontend.detailProduct', compact('product'));
            } else {
                return redirect('/')->with('status', 'The link was broken');
            }
        } else {
            return view('/')->with('status', 'No category found');
        }
    }
}
