<div>
    <div class="container">
        <div class="card">
            <div class="card-header">{{ __('Post Add') }}</div>
            <div class="card-body">
                <div class="alert @if (!empty(session('alert'))) alert-{{ session('alert') }} @else d-none @endif">
                    @if (!empty(session('msg')))
                        {{ session('msg') }}
                    @endif
                </div>
                <form wire:submit.prevent="save">
                    <div class="form-group mb-3 row d-flex align-items-center">
                        <label class="col-12 col-md-2">{{ __('Title') }}</label>
                        <div class="col-12 col-md-10">
                            <input wire:model="title" type="text"
                                class="form-control @error('title') is-invalid @enderror">
                            <div class="invalid-feedback">
                                @error('title')
                                    {{ $message }}
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="form-group mb-3 row d-flex align-items-center">
                        <label class="col-12 col-md-2">{{ __('Content') }}</label>
                        <div class="col-12 col-md-10">
                            <textarea wire:model="content" type="text" class="form-control @error('content') is-invalid @enderror"></textarea>
                            <div class="invalid-feedback">
                                @error('content')
                                    {{ $message }}
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="text-end">
                        <a class="btn btn-sm btn-secondary" href="">Batal</a>
                        <button class="btn btn-sm btn-primary" type="submit">{{ __('Save') }}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
